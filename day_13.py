import argparse
from typing import TextIO
from enum import Enum, auto


class Paper:
    def __init__(self, points):
        self.points = points

    def fold_horizontal(self, position):
        new_points = []
        while len(self.points) != 0:
            new_point = self.points.pop()
            new_point = self._fold_point_horizontal(new_point, position)
            if new_point not in new_points:
                new_points.append(new_point)
        self.points = new_points

    def fold_vertical(self, position):
        new_points = []
        while len(self.points) != 0:
            new_point = self.points.pop()
            new_point = self._fold_point_vertical(new_point, position)
            if new_point not in new_points:
                new_points.append(new_point)
        self.points = new_points

    def _fold_point_horizontal(self, point, position):
        x, y = point
        if y < position:
            return x, y
        distance = y - position
        y = position - distance
        return x, y

    def _fold_point_vertical(self, point, position):
        x, y = point
        if x < position:
            return x, y
        distance = x - position
        x = position - distance
        return x, y

    def count_points(self):
        return len(self.points)

    def __str__(self):
        width = max(self.points, key=lambda p: p[0])[0] + 1
        height = max(self.points, key=lambda p: p[1])[1] + 1
        canvas = ''
        for y in range(height):
            for x in range(width):
                if (x, y) in self.points:
                    canvas += '#'
                else:
                    canvas += ' '
            canvas += '\n'

        return canvas


class Direction(Enum):
    HORIZONTAL = auto()
    VERTICAL = auto()


class Instruction:
    def __init__(self, direction, position):
        self.direction = direction
        self.position = position


def part_1(input_file: TextIO):
    points = []
    instructions = []
    state = 'points'
    for line in input_file.readlines():
        line = line.strip()
        if state == 'points':
            if line == '':
                state = 'instructions'
                continue
            x, y = [int(p) for p in line.split(',')]
            points.append((x, y))
        elif state == 'instructions':
            text, value = line.split('=')
            instructions.append(Instruction(Direction.HORIZONTAL if text[-1] == 'y' else Direction.VERTICAL,
                                            int(value)))

    paper = Paper(points)
    instruction = instructions[0]
    if instruction.direction == Direction.HORIZONTAL:
        paper.fold_horizontal(instruction.position)
    else:
        paper.fold_vertical(instruction.position)

    return paper.count_points()


def part_2(input_file: TextIO):
    points = []
    instructions = []
    state = 'points'
    for line in input_file.readlines():
        line = line.strip()
        if state == 'points':
            if line == '':
                state = 'instructions'
                continue
            x, y = [int(p) for p in line.split(',')]
            points.append((x, y))
        elif state == 'instructions':
            text, value = line.split('=')
            instructions.append(Instruction(Direction.HORIZONTAL if text[-1] == 'y' else Direction.VERTICAL,
                                            int(value)))

    paper = Paper(points)
    for instruction in instructions:
        if instruction.direction == Direction.HORIZONTAL:
            paper.fold_horizontal(instruction.position)
        else:
            paper.fold_vertical(instruction.position)

    return paper


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 13')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
